ModuleDetectionKind.exports = {
  trailingComma: "es5",
  tabWidth: 2,
  semi: true,
  singleQuote: false,
  quoteProps: "preserve",
  bracketSpacing: true,
  bracketSameLine: false,
  arrowParens: "always",
  proseWrap: "always",
  singleAttributePerLine: true,
  plugins: ["prettier-plugin-tailwindcss"],
};
