title: The KDE Plasma desktop, in an immutable fashion
leadin: Surf the web, keep in touch with friends, manage files, enjoy music and video, and get productive at work without having to worry about breaking your system.

image:
  src: https://stg.fedoraproject.org/assets/images/fedora-kinoite-light.png
  alt: Fedora Kinoite Image
  width: 400
  height: 300
header_images:
  - image: assets/images/immutable/laptop.svg
    alt_text: Screenshot of blank laptop as background to KDE desktop
  - image: assets/images/immutable/kde-desktop.jpg
    alt_text: Screenshot of KDE Desktop
links:
  - text: Download Now
    url: /kinoite/download/
  - text: Documentation
    url: "https://docs.fedoraproject.org/en-US/fedora-kinoite/"
sections:
  - sectionTitle: Why Fedora Kinoite?
    content:
      - title: Reliable
        description: Each version is updated for approximately 13 months, and each update takes effect on your next reboot, keeping your system consistent. You can even keep working while the updates are being applied!
      - title: Atomic
        description: The whole system is updated in one go, and an update will not apply if anything goes wrong, meaning you will <i>always</i> have a working computer.
      - title: Safe
        description: A previous version of your system is always kept around, just in case. If you need to go back in time, you can!
      - title: Containerized
        description: Graphical applications are installed via Flatpak, and keep themselves separate from the base system. They also allow for fine-grained control over their permissions.
      - title: Developer-Friendly
        description: Toolbx keeps all of your development tools neatly organized per-project. Keep multiple versions of your tools independent from each other and unaffected by changes to the base system.
      - title: Private, Trusted, Open Source
        description: There are no ads, and all your data belongs to you! Fedora is built on the latest open source technologies, and backed by Red Hat.
  - sectionTitle: Built On Top of the Best
    content:
      - title: KDE Plasma Desktop
        description: Includes the latest versions of the modern and modular desktop, built by the KDE community.
        image: public/assets/images/immutable/kde-desktop-square.jpg
      - title: Applications as Flatpaks
        description: Download thousands of open-source and proprietary, containerized applications thanks to Flatpak and Flathub.
        image: public/assets/images/immutable/flatpak-apps-faded-square.png
      - title: rpm-ostree
        description: rpm-ostree is a hybrid image/package system. It combines libostree and libdnf to provide atomic and safe upgrades with local RPM package layering.
        image: public/assets/images/immutable/rpm-ostree-square.png
      - title: Toolbx for Development
        description: Easily create OCI containers which are fully-integrated with your host system, enabling seamless usage of both GUI and CLI tools.
        image: public/assets/images/immutable/toolbox-square.png
      - title: Fedora Community & Support
        description: Fedora creates an innovative, free and open source platform for hardware, clouds, and containers that enables software developers and community members to build tailored solutions for their users.
        image: public/assets/images/immutable/fedora-square.png
  - sectionTitle: Related Projects
    content:
      - title:
        description: An immutable desktop operating system featuring the modern and elegant GNOME Desktop
        image: public/assets/images/fedora-silverblue-light.png
        url: /silverblue
      - title:
        description: An immutable desktop operating system featuring the keyboard-driven Sway window manager.
        image: public/assets/images/fedora-sericea-light.png
        url: /sericea
      - title:
        description: Fedora CoreOS Fedora CoreOS is an automatically updating, minimal, monolithic, container-focused operating system, designed for clusters but also operable standalone, optimized for Kubernetes but also great without it.
        image: public/assets/images/coreos-logo-light.png
        url: /coreos
