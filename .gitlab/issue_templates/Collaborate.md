# New collaborator

Welcome and thanks for considering contributing to Fedora! If you have not already done so, it is recommended to link your GitLab account with your Fedora account. Linking your account has the following benefits.

- Fedora's [Badges](https://badges.fedoraproject.org/) system will be able to award you a badge for contributions you make.
- If you have added your [Fedora account](https://accounts.fedoraproject.org/) in the Online Accounts section of GNOME Settings (or otherwise have a valid `<username>@FEDORAPROJECT.ORG` Kerberos ticket from [Fedora's central authentication service](https://id.fedoraproject.org/)), GitLab will automatically sign you in when you browse to this site using the [Fedora GitLab single sign-on link](https://gitlab.com/groups/fedora/-/saml/sso).
- On rare occasion, permissions may be reset and non-linked GitLab accounts may lose access to things. If this happens, you may need to re-request access to this repo.

Perform the following steps to link your GitLab account with your Fedora account.

1. Create or sign in with your GitLab account.
2. Go to https://gitlab.com/groups/fedora/-/saml/sso and sign in with your Fedora account.
3. Follow the prompts to link your Fedora account to your GitLab account.

By clicking the `Create Issue` button below, you agree to the following terms.

- [x] I agree to the terms of the [Fedora Project Contributor Agreement](https://docs.fedoraproject.org/en-US/legal/fpca/).
- [x] I will adhere to the [Fedora Community Code of Conduct](https://docs.fedoraproject.org/en-US/project/code-of-conduct/).

A GitLab admin will leave a follow-up comment on this issue to let you know when your account has been granted write access to the [fedora-websites-3.0](https://gitlab.com/fedora/websites-apps/fedora-websites/fedora-websites-3.0) repo.